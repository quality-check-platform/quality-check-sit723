const { Config } = require('@foal/core');

module.exports = {
  type: Config.getOrThrow('database.type', 'string'),

  url: Config.get('database.url', 'string'),
  logging: process.env.NODE_ENV === 'production'
  ? ["error", "schema"]
  : true,
  schema: "systemmaster",
  cache: true,
  
  dropSchema: Config.get('database.dropSchema', 'boolean', false),
  synchronize: Config.get('database.synchronize', 'boolean', false),

  entities: ["build/app/**/*.entity.js"],
  migrations: ["build/migrations/*.js"],
  cli: {
    migrationsDir: "src/migrations"
  },
}