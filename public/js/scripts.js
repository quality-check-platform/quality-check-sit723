(function () {
	'use strict'
  
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.querySelectorAll('.needs-validation')
  
	// Loop over them and prevent submission
	Array.prototype.slice.call(forms)
	  .forEach(function (form) {
		form.addEventListener('submit', function (event) {
		  if (!form.checkValidity()) {
			event.preventDefault()
			event.stopPropagation()
		  }
		  form.classList.add('was-validated')
		}, false)
	  })
  })()


$.when( $.ready ).then(function() {
	// Document is ready.
	$( "#checkForm" ).submit(function( event ) {
	  });

	const urlParams = new URLSearchParams(window.location.search);
	const barcode = "" + urlParams.get('barcode');
	if(barcode != "" && barcode != "null") {
		$("#batchid").val(barcode);
		handlecheckForm();
	}

  });
  
  async function handlecheckForm()
  {

	var batchid = ("" + $("#batchid").val()).substr(0,100);
	var jqxhr = $.get( "/v1/batch/"+batchid)
		.done(function( data ) {
			$( "#resultscontainer" ).removeClass("d-none");
			$( "#searchresults" ).removeClass("d-none");
			populateresults(data);
		})
		.fail(function( data ) {
			if(data.status == 404 | data.status == 400) { // show the error message section
				$( "#resultscontainer" ).removeClass("d-none");
				$( "#batchNotFound" ).removeClass("d-none");
			}
			else {
				// TODO: handle other errors.
			}
		  console.log(data );
		})
		.always(function() {
			console.log("always");

			$([document.documentElement, document.body]).animate({
				scrollTop: $("#resultscontainer").offset().top
			}, 200);
			  });
  }

  function populateresults(data) {
console.log("updating details");
	//TODO: error checking here for correct structure
	// Product
	$( "#productname" ).text(data[0][0].fk_batch.fk_product.productname + " (" + data[0][0].fk_batch.catchstatus + ")");

	// Source
	$( "#caughtcompany" ).text("Fishery: " + data[0][0].fk_custodian.custodianname);
	$( "#caughtlocation" ).text("Location: " + data[0][0].fk_location.city + ", " + data[0][0].fk_location.state + ", " + data[0][0].fk_location.country);
	$( "#caughtdate" ).text("Date Caught: " + dayjs(data[0][0].movementtz).format('DD/MM/YYYY'));
	if(""+data[0][0].fk_custodian.companycompliance != "null") {$( "#companycompliance" ).text("Company Compliance: " + data[0][0].fk_custodian.companycompliance);}
	$( "#companyurl" ).html("Website: <a target=\"_blank\" href=\"" + data[0][0].fk_custodian.companyurl + "\">" + data[0][0].fk_custodian.custodianname + "</a>");

	// Travels
	var countalarms = 0;
	var prevcustodian = "";
	var listitem = ""
	var movementdate;
	var hasalarms = false;
	for(var i = 0; i < data[0].length; i++ ) {

		movementdate = dayjs(data[0][i].movementtz);

		if(prevcustodian != data[0][i].fk_custodian.custodianname) {
			if(listitem != "") {
				if(hasalarms){
					listitem = "<i class=\"bi bi-exclamation-diamond\" style=\"padding-right: 1em; color: orange;\"></i>" + listitem;
				}
				else{
					listitem = "<i class=\"bi bi-check-circle\" style=\"padding-right: 1em; color: green;\"></i>" + listitem;
				}
				$("#movementslist").append("<li class=\"list-group-item\">" + listitem + "</li>");
				listitem = " ";
				hasalarms = false;
			}

			listitem += movementdate.format('DD/MM/YYYY') + " - ";
			listitem += data[0][i].fk_custodian.custodianname + " ";
			listitem += " (" + data[0][i].fk_location.city + ", " + data[0][i].fk_location.state + ", " + data[0][i].fk_location.country + ")";
		}

		if(data[0][i].alarms && data[0][i].alarms.length > 0) {	hasalarms = true; countalarms += data[0][i].alarms.length; }

		prevcustodian = data[0][i].fk_custodian.custodianname;
	}
	if(listitem != "") {
		if(hasalarms){
			listitem = "<i class=\"bi bi-exclamation-diamond\" style=\"padding-right: 1em; color: orange;\"></i>" + listitem;
		}
		else{
			listitem = "<i class=\"bi bi-check-circle\" style=\"padding-right: 1em; color: green;\"></i>" + listitem;
		}
		$("#movementslist").append("<li class=\"list-group-item\">" + listitem + "</li>");
		listitem = "";
	}

	// Quality
	if(countalarms == 0) {
		$("#movementslist").append("<li class=\"list-group-item list-group-item-success\">Your product has had no alarms and has been kept within optimal conditions at all time.</li>");
	}
	else {
		$("#movementslist").append("<li class=\"list-group-item list-group-item-warning\">Your product has had " + countalarms + " alarms. Please check the Dynamic Shelf Life for details.</li>");
	}

  }