import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630969325749 implements MigrationInterface {
    name = 'migration1630969325749'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" DROP COLUMN "singularity"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" DROP COLUMN "sensoridtransform"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" DROP COLUMN "dtpath"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" DROP COLUMN "dttransform"`);
        await queryRunner.query(`ALTER TABLE "appdata"."eventtype" DROP COLUMN "eventtypestructure"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."eventtype" ADD "eventtypestructure" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" ADD "dttransform" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" ADD "dtpath" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" ADD "sensoridtransform" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" ADD "singularity" citext NOT NULL`);
    }

}
