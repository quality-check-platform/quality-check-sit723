import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631408156983 implements MigrationInterface {
    name = 'migration1631408156983'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "alarmtemp"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "eventtypename" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "alarmthreshold" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ALTER COLUMN "alarmqty" SET DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ALTER COLUMN "alarmvolume" SET DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ALTER COLUMN "alarmvolume" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ALTER COLUMN "alarmqty" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "alarmthreshold"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "eventtypename"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "alarmtemp" integer`);
    }

}
