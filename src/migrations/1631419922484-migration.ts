import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631419922484 implements MigrationInterface {
    name = 'migration1631419922484'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "alarmkey" character varying`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD "alarmtz" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD "eventtypename" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD "alarmvalue" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD "locationidLocationid" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD CONSTRAINT "FK_0823258c596604e972b5b8e8440" FOREIGN KEY ("locationidLocationid") REFERENCES "appdata"."location"("locationid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP CONSTRAINT "FK_0823258c596604e972b5b8e8440"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP COLUMN "locationidLocationid"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP COLUMN "alarmvalue"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP COLUMN "eventtypename"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP COLUMN "alarmtz"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "alarmkey"`);
    }

}
