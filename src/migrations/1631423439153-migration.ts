import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631423439153 implements MigrationInterface {
    name = 'migration1631423439153'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP COLUMN "alarmpoint"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD "alarmpoint" numeric`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP COLUMN "alarmpoint"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD "alarmpoint" integer`);
    }

}
