import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631410868028 implements MigrationInterface {
    name = 'migration1631410868028'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD "eventtypename" citext NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD "alarmconfig" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP COLUMN "alarmconfig"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP COLUMN "eventtypename"`);
    }

}
