import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630717240552 implements MigrationInterface {
    name = 'migration1630717240552'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" RENAME COLUMN "eventtypename" TO "sensoreventtypename"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" RENAME COLUMN "sensortype" TO "sensortypename"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" RENAME COLUMN "sensortypename" TO "sensortype"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" RENAME COLUMN "sensoreventtypename" TO "eventtypename"`);
    }

}
