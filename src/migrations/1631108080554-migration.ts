import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631108080554 implements MigrationInterface {
    name = 'migration1631108080554'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" RENAME COLUMN "custodianame" TO "custodianname"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" RENAME COLUMN "custodianname" TO "custodianame"`);
    }

}
