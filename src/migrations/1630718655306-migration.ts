import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630718655306 implements MigrationInterface {
    name = 'migration1630718655306'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" ADD "fkSensortypeSensortypeid" uuid`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" ADD CONSTRAINT "FK_5fd97f5795901cc3437c2763487" FOREIGN KEY ("fkSensortypeSensortypeid") REFERENCES "appdata"."sensortype"("sensortypeid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" DROP CONSTRAINT "FK_5fd97f5795901cc3437c2763487"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" DROP COLUMN "fkSensortypeSensortypeid"`);
    }

}
