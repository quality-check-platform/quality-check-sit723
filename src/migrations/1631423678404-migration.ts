import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631423678404 implements MigrationInterface {
    name = 'migration1631423678404'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD "fkProductProductid" uuid NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_438df41503c9dcfc4d32448f7f" ON "appdata"."productqualitymodel" ("catchstatus", "eventtypename", "fkProductProductid", "alarmpoint") `);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" ADD CONSTRAINT "FK_6fa012e0a738323c4fce7cc375c" FOREIGN KEY ("fkProductProductid") REFERENCES "appdata"."product"("productid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP CONSTRAINT "FK_6fa012e0a738323c4fce7cc375c"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_438df41503c9dcfc4d32448f7f"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" DROP COLUMN "fkProductProductid"`);
    }

}
