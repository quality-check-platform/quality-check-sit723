import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631409201005 implements MigrationInterface {
    name = 'migration1631409201005'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_ed3bdc6da0b4f26e3b28488a9f" ON "appdata"."productconfig" ("fkProductProductid", "catchstatus") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "appdata"."IDX_ed3bdc6da0b4f26e3b28488a9f"`);
    }

}
