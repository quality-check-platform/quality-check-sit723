import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630972953850 implements MigrationInterface {
    name = 'migration1630972953850'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" ADD "singularity" citext NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype" DROP COLUMN "singularity"`);
    }

}
