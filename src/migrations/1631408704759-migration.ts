import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631408704759 implements MigrationInterface {
    name = 'migration1631408704759'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD "catchstatus" citext`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP COLUMN "catchstatus"`);
    }

}
