import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631422731604 implements MigrationInterface {
    name = 'migration1631422731604'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" RENAME COLUMN "temp" TO "modelpoint"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" RENAME COLUMN "modelpoint" TO "temp"`);
    }

}
