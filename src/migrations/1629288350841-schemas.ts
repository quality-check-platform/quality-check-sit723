// schema systemmaster must be manually created before running this first script

import {MigrationInterface, QueryRunner} from "typeorm";

export class schemas1629288350841 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createSchema("appdata", true);
		await queryRunner.createSchema("systemadmin", true);
   }

    public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropSchema("appdata",  true, true);
		await queryRunner.dropSchema("systemadmin",  true, true);
    }

}
