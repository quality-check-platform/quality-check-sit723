import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630717799982 implements MigrationInterface {
    name = 'migration1630717799982'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_9773b9c1041cb21ae7660254f2" ON "appdata"."sensortype_lnk_eventtype" ("sensoreventtypename") `);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_f1530545f684ef1d2c133e41c0" ON "appdata"."sensortype" ("sensortypename") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "appdata"."IDX_f1530545f684ef1d2c133e41c0"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_9773b9c1041cb21ae7660254f2"`);
    }

}
