import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631422812037 implements MigrationInterface {
    name = 'migration1631422812037'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" RENAME COLUMN "modelpoint" TO "alarmpoint"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productqualitymodel" RENAME COLUMN "alarmpoint" TO "modelpoint"`);
    }

}
