import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631405862263 implements MigrationInterface {
    name = 'migration1631405862263'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "appdata"."productqualitymodel_productconfigstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."productqualitymodel" ("productqualitymodelid" uuid NOT NULL DEFAULT uuid_generate_v4(), "catchstatus" citext, "temp" integer, "shelflifereductionrate" integer, "productconfigStatus" "appdata"."productqualitymodel_productconfigstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_db1847ae550e6baaf11dbab0f4a" PRIMARY KEY ("productqualitymodelid"))`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "catchstatus" citext`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "alarmtemp" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "shelflife" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD "fkProductProductid" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD "alarmqty" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD "alarmvolume" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD "shelflife" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" ADD "catchstatus" citext`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" DROP COLUMN "lat"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" ADD "lat" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" DROP COLUMN "long"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" ADD "long" integer`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" ADD CONSTRAINT "FK_2bcc5b228a447ff07eeb8e01638" FOREIGN KEY ("fkProductProductid") REFERENCES "appdata"."product"("productid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP CONSTRAINT "FK_2bcc5b228a447ff07eeb8e01638"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" DROP COLUMN "long"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" ADD "long" citext`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" DROP COLUMN "lat"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" ADD "lat" citext`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" DROP COLUMN "catchstatus"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP COLUMN "shelflife"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP COLUMN "alarmvolume"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP COLUMN "alarmqty"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "fkProductProductid"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "shelflife"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "alarmtemp"`);
        await queryRunner.query(`ALTER TABLE "appdata"."productconfig" DROP COLUMN "catchstatus"`);
        await queryRunner.query(`DROP TABLE "appdata"."productqualitymodel"`);
        await queryRunner.query(`DROP TYPE "appdata"."productqualitymodel_productconfigstatus_enum"`);
    }

}
