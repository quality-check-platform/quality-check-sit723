import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630715903495 implements MigrationInterface {
    name = 'migration1630715903495'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" ADD "eventtypename" citext NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" DROP COLUMN "eventtypename"`);
    }

}
