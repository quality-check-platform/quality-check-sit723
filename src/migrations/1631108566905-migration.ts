import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631108566905 implements MigrationInterface {
    name = 'migration1631108566905'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" ADD "companycompliance" citext`);
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" ADD "companyurl" citext`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" DROP COLUMN "companyurl"`);
        await queryRunner.query(`ALTER TABLE "appdata"."custodian" DROP COLUMN "companycompliance"`);
    }

}
