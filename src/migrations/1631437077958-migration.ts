import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1631437077958 implements MigrationInterface {
    name = 'migration1631437077958'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP CONSTRAINT "FK_0823258c596604e972b5b8e8440"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" RENAME COLUMN "locationidLocationid" TO "movementidMovementid"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD CONSTRAINT "FK_2a52b99dbbf97b09d243803d0ad" FOREIGN KEY ("movementidMovementid") REFERENCES "appdata"."movement"("movementid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP CONSTRAINT "FK_2a52b99dbbf97b09d243803d0ad"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" RENAME COLUMN "movementidMovementid" TO "locationidLocationid"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD CONSTRAINT "FK_0823258c596604e972b5b8e8440" FOREIGN KEY ("locationidLocationid") REFERENCES "appdata"."location"("locationid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
