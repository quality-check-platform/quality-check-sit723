import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1630713385350 implements MigrationInterface {
    name = 'migration1630713385350'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "systemmaster"."permission" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "codeName" character varying(100) NOT NULL, CONSTRAINT "UQ_88aa7accff3f2d2077b823a941a" UNIQUE ("codeName"), CONSTRAINT "PK_6f184e2c7729fbbc44cae461a52" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "systemmaster"."group" ("id" SERIAL NOT NULL, "name" character varying(80) NOT NULL, "codeName" character varying(100) NOT NULL, CONSTRAINT "UQ_04594c9ffafdcb4808377353f2c" UNIQUE ("codeName"), CONSTRAINT "PK_47e0650803292ee9fee4e0e8f04" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."user_userstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."user" ("id" SERIAL NOT NULL, "email" citext NOT NULL, "password" character varying, "name" citext NOT NULL, "userStatus" "appdata"."user_userstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkCustodianCustodianid" uuid NOT NULL, CONSTRAINT "UQ_bc45b0fb2ebd28a741d1ae291f0" UNIQUE ("email"), CONSTRAINT "PK_3196dcf63b77b0459312e3d267d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."sensor_sensorstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."sensor" ("sensorid" uuid NOT NULL DEFAULT uuid_generate_v4(), "sensorexternalid" citext NOT NULL, "sensorStatus" "appdata"."sensor_sensorstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkLocationLocationid" uuid, CONSTRAINT "PK_07e16d8b05f2cfd55d60f35fb3f" PRIMARY KEY ("sensorid"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_a61c4df6102ad981264f5487ec" ON "appdata"."sensor" ("sensorexternalid") `);
        await queryRunner.query(`CREATE TYPE "appdata"."product_productstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."product" ("productid" uuid NOT NULL DEFAULT uuid_generate_v4(), "productname" citext NOT NULL, "productontology" citext NOT NULL, "productstatus" "appdata"."product_productstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_90deadd4f7371f241f72010a469" PRIMARY KEY ("productid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."productconfig_productconfigstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."productconfig" ("productconfigid" uuid NOT NULL DEFAULT uuid_generate_v4(), "productconfigStatus" "appdata"."productconfig_productconfigstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_94245060716109c064254651749" PRIMARY KEY ("productconfigid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."custodian_custodianstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."custodian" ("custodianid" uuid NOT NULL DEFAULT uuid_generate_v4(), "custodianame" citext NOT NULL, "registrationnumber" citext, "street1" citext, "street2" citext, "city" citext, "state" citext, "postcode" citext, "country" citext, "custodianstatus" "appdata"."custodian_custodianstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_02bb2030ff8af2484e0df0c061b" PRIMARY KEY ("custodianid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."location_locationstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."location" ("locationid" uuid NOT NULL DEFAULT uuid_generate_v4(), "locationname" citext NOT NULL, "locationtype" citext NOT NULL, "lat" citext, "long" citext, "city" citext, "state" citext, "postcode" citext, "country" citext, "locationstatus" "appdata"."location_locationstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkCustodianCustodianid" uuid NOT NULL, CONSTRAINT "PK_9f4e17c9b65b8640661bce2a3b0" PRIMARY KEY ("locationid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."batch_batchstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."batch" ("batchid" uuid NOT NULL DEFAULT uuid_generate_v4(), "batchcreatedtz" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "barcode" citext, "batchstatus" "appdata"."batch_batchstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkProductProductid" uuid NOT NULL, "fkCustodianCustodianid" uuid NOT NULL, "fkLocationLocationid" uuid NOT NULL, CONSTRAINT "PK_44179697befae1e826ca29d446e" PRIMARY KEY ("batchid"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_110b0e714798bbe6a68a0f8053" ON "appdata"."batch" ("barcode") `);
        await queryRunner.query(`CREATE TYPE "appdata"."sensortype_lnk_eventtype_lnkstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."sensortype_lnk_eventtype" ("id" SERIAL NOT NULL, "datamap" citext NOT NULL, "lnkStatus" "appdata"."sensortype_lnk_eventtype_lnkstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkSensortypeSensortypeid" uuid NOT NULL, "fkEventtypeEventtypeid" uuid NOT NULL, CONSTRAINT "PK_1c6e684ddec4e3aded32699e2e0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."sensortype_sensortypestatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."sensortype" ("sensortypeid" uuid NOT NULL DEFAULT uuid_generate_v4(), "sensortype" citext NOT NULL, "datatype" citext NOT NULL, "singularity" citext NOT NULL, "sensoridpath" citext NOT NULL, "sensoridtransform" citext NOT NULL, "dtpath" citext NOT NULL, "dttransform" citext NOT NULL, "sensortypeStatus" "appdata"."sensortype_sensortypestatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_41295a9b500eeed6892c1723918" PRIMARY KEY ("sensortypeid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."movement_movementstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."movement" ("movementid" uuid NOT NULL DEFAULT uuid_generate_v4(), "movementtype" citext NOT NULL, "movementtz" TIMESTAMP WITH TIME ZONE NOT NULL, "movementstatus" "appdata"."movement_movementstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "fkCustodianCustodianid" uuid NOT NULL, "fkLocationLocationid" uuid NOT NULL, "fkBatchBatchid" uuid NOT NULL, CONSTRAINT "PK_7c7a299260fbecc7fbc616016fd" PRIMARY KEY ("movementid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."eventtype_eventtypestatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."eventtype" ("eventtypeid" uuid NOT NULL DEFAULT uuid_generate_v4(), "eventtypename" citext NOT NULL, "eventtypestructure" citext NOT NULL, "eventtypeStatus" "appdata"."eventtype_eventtypestatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_f44726c2988783cbe98b43b2a9e" PRIMARY KEY ("eventtypeid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."productalarm_productconfigstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."productalarm" ("productconfigid" uuid NOT NULL DEFAULT uuid_generate_v4(), "productconfigStatus" "appdata"."productalarm_productconfigstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, CONSTRAINT "PK_5c12f63175b74af9848a71049c6" PRIMARY KEY ("productconfigid"))`);
        await queryRunner.query(`CREATE TYPE "appdata"."alarm_alarmstatus_enum" AS ENUM('Current', 'Archived', 'Draft')`);
        await queryRunner.query(`CREATE TABLE "appdata"."alarm" ("alarmid" uuid NOT NULL DEFAULT uuid_generate_v4(), "alarmStatus" "appdata"."alarm_alarmstatus_enum" NOT NULL DEFAULT 'Current', "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastUpdated" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "entityVersion" integer NOT NULL, "batchidBatchid" uuid NOT NULL, CONSTRAINT "PK_863bf96cf2e71f77c85be2b775a" PRIMARY KEY ("alarmid"))`);
        await queryRunner.query(`CREATE TABLE "systemmaster"."group_permissions_permission" ("groupId" integer NOT NULL, "permissionId" integer NOT NULL, CONSTRAINT "PK_5bc55c3453030301b10bf8a721d" PRIMARY KEY ("groupId", "permissionId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_967412db540f54fcbe51dd8665" ON "systemmaster"."group_permissions_permission" ("groupId") `);
        await queryRunner.query(`CREATE INDEX "IDX_68542db92fa23536c6896a1d38" ON "systemmaster"."group_permissions_permission" ("permissionId") `);
        await queryRunner.query(`CREATE TABLE "appdata"."user_groups_group" ("userId" integer NOT NULL, "groupId" integer NOT NULL, CONSTRAINT "PK_d87ad77f47faeae7a15e5dd5bbe" PRIMARY KEY ("userId", "groupId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_6f50686402cb9b12cc31638222" ON "appdata"."user_groups_group" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_e2c76613ff6e4b3a24847efdff" ON "appdata"."user_groups_group" ("groupId") `);
        await queryRunner.query(`CREATE TABLE "appdata"."user_user_permissions_permission" ("userId" integer NOT NULL, "permissionId" integer NOT NULL, CONSTRAINT "PK_2dac28bfbe2308b37629a8925aa" PRIMARY KEY ("userId", "permissionId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_1b9de2bbc34302843e8fc88bf8" ON "appdata"."user_user_permissions_permission" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_01121bcf977c2b77d4845cbbae" ON "appdata"."user_user_permissions_permission" ("permissionId") `);
        await queryRunner.query(`ALTER TABLE "appdata"."user" ADD CONSTRAINT "FK_7a9f77a50125cc0796eb94bf4bf" FOREIGN KEY ("fkCustodianCustodianid") REFERENCES "appdata"."custodian"("custodianid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" ADD CONSTRAINT "FK_a18733c6de04c25271636baf69e" FOREIGN KEY ("fkLocationLocationid") REFERENCES "appdata"."location"("locationid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" ADD CONSTRAINT "FK_150faa546c28d203275b45b8f20" FOREIGN KEY ("fkCustodianCustodianid") REFERENCES "appdata"."custodian"("custodianid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD CONSTRAINT "FK_618fe32f2ea51c4973a8f4e9c54" FOREIGN KEY ("fkProductProductid") REFERENCES "appdata"."product"("productid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD CONSTRAINT "FK_209a8e6bb1e2f28ccfba9529416" FOREIGN KEY ("fkCustodianCustodianid") REFERENCES "appdata"."custodian"("custodianid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" ADD CONSTRAINT "FK_672ce6443a95c3e5fb4e209e730" FOREIGN KEY ("fkLocationLocationid") REFERENCES "appdata"."location"("locationid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" ADD CONSTRAINT "FK_75305540bb3ef8dcb3e8f9c7a44" FOREIGN KEY ("fkSensortypeSensortypeid") REFERENCES "appdata"."sensortype"("sensortypeid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" ADD CONSTRAINT "FK_334d9d296fb0cafea23cd1d093c" FOREIGN KEY ("fkEventtypeEventtypeid") REFERENCES "appdata"."eventtype"("eventtypeid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" ADD CONSTRAINT "FK_cdec330c8eed4c41b375df5f6ce" FOREIGN KEY ("fkCustodianCustodianid") REFERENCES "appdata"."custodian"("custodianid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" ADD CONSTRAINT "FK_eefdbebcd0bfaa5414f8e7762b8" FOREIGN KEY ("fkLocationLocationid") REFERENCES "appdata"."location"("locationid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" ADD CONSTRAINT "FK_d207658865f6098f51276e573ed" FOREIGN KEY ("fkBatchBatchid") REFERENCES "appdata"."batch"("batchid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" ADD CONSTRAINT "FK_17d349379c1764f29d6e37ef54f" FOREIGN KEY ("batchidBatchid") REFERENCES "appdata"."batch"("batchid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "systemmaster"."group_permissions_permission" ADD CONSTRAINT "FK_967412db540f54fcbe51dd8665b" FOREIGN KEY ("groupId") REFERENCES "systemmaster"."group"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "systemmaster"."group_permissions_permission" ADD CONSTRAINT "FK_68542db92fa23536c6896a1d384" FOREIGN KEY ("permissionId") REFERENCES "systemmaster"."permission"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_groups_group" ADD CONSTRAINT "FK_6f50686402cb9b12cc31638222d" FOREIGN KEY ("userId") REFERENCES "appdata"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_groups_group" ADD CONSTRAINT "FK_e2c76613ff6e4b3a24847efdff2" FOREIGN KEY ("groupId") REFERENCES "systemmaster"."group"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_user_permissions_permission" ADD CONSTRAINT "FK_1b9de2bbc34302843e8fc88bf8a" FOREIGN KEY ("userId") REFERENCES "appdata"."user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_user_permissions_permission" ADD CONSTRAINT "FK_01121bcf977c2b77d4845cbbaee" FOREIGN KEY ("permissionId") REFERENCES "systemmaster"."permission"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`CREATE TABLE "systemmaster"."query-result-cache" ("id" SERIAL NOT NULL, "identifier" character varying, "time" bigint NOT NULL, "duration" integer NOT NULL, "query" text NOT NULL, "result" text NOT NULL, CONSTRAINT "PK_d4f2c4f1ea623e823677faeba89" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "systemmaster"."query-result-cache"`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_user_permissions_permission" DROP CONSTRAINT "FK_01121bcf977c2b77d4845cbbaee"`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_user_permissions_permission" DROP CONSTRAINT "FK_1b9de2bbc34302843e8fc88bf8a"`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_groups_group" DROP CONSTRAINT "FK_e2c76613ff6e4b3a24847efdff2"`);
        await queryRunner.query(`ALTER TABLE "appdata"."user_groups_group" DROP CONSTRAINT "FK_6f50686402cb9b12cc31638222d"`);
        await queryRunner.query(`ALTER TABLE "systemmaster"."group_permissions_permission" DROP CONSTRAINT "FK_68542db92fa23536c6896a1d384"`);
        await queryRunner.query(`ALTER TABLE "systemmaster"."group_permissions_permission" DROP CONSTRAINT "FK_967412db540f54fcbe51dd8665b"`);
        await queryRunner.query(`ALTER TABLE "appdata"."alarm" DROP CONSTRAINT "FK_17d349379c1764f29d6e37ef54f"`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" DROP CONSTRAINT "FK_d207658865f6098f51276e573ed"`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" DROP CONSTRAINT "FK_eefdbebcd0bfaa5414f8e7762b8"`);
        await queryRunner.query(`ALTER TABLE "appdata"."movement" DROP CONSTRAINT "FK_cdec330c8eed4c41b375df5f6ce"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" DROP CONSTRAINT "FK_334d9d296fb0cafea23cd1d093c"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensortype_lnk_eventtype" DROP CONSTRAINT "FK_75305540bb3ef8dcb3e8f9c7a44"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP CONSTRAINT "FK_672ce6443a95c3e5fb4e209e730"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP CONSTRAINT "FK_209a8e6bb1e2f28ccfba9529416"`);
        await queryRunner.query(`ALTER TABLE "appdata"."batch" DROP CONSTRAINT "FK_618fe32f2ea51c4973a8f4e9c54"`);
        await queryRunner.query(`ALTER TABLE "appdata"."location" DROP CONSTRAINT "FK_150faa546c28d203275b45b8f20"`);
        await queryRunner.query(`ALTER TABLE "appdata"."sensor" DROP CONSTRAINT "FK_a18733c6de04c25271636baf69e"`);
        await queryRunner.query(`ALTER TABLE "appdata"."user" DROP CONSTRAINT "FK_7a9f77a50125cc0796eb94bf4bf"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_01121bcf977c2b77d4845cbbae"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_1b9de2bbc34302843e8fc88bf8"`);
        await queryRunner.query(`DROP TABLE "appdata"."user_user_permissions_permission"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_e2c76613ff6e4b3a24847efdff"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_6f50686402cb9b12cc31638222"`);
        await queryRunner.query(`DROP TABLE "appdata"."user_groups_group"`);
        await queryRunner.query(`DROP INDEX "systemmaster"."IDX_68542db92fa23536c6896a1d38"`);
        await queryRunner.query(`DROP INDEX "systemmaster"."IDX_967412db540f54fcbe51dd8665"`);
        await queryRunner.query(`DROP TABLE "systemmaster"."group_permissions_permission"`);
        await queryRunner.query(`DROP TABLE "appdata"."alarm"`);
        await queryRunner.query(`DROP TYPE "appdata"."alarm_alarmstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."productalarm"`);
        await queryRunner.query(`DROP TYPE "appdata"."productalarm_productconfigstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."eventtype"`);
        await queryRunner.query(`DROP TYPE "appdata"."eventtype_eventtypestatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."movement"`);
        await queryRunner.query(`DROP TYPE "appdata"."movement_movementstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."sensortype"`);
        await queryRunner.query(`DROP TYPE "appdata"."sensortype_sensortypestatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."sensortype_lnk_eventtype"`);
        await queryRunner.query(`DROP TYPE "appdata"."sensortype_lnk_eventtype_lnkstatus_enum"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_110b0e714798bbe6a68a0f8053"`);
        await queryRunner.query(`DROP TABLE "appdata"."batch"`);
        await queryRunner.query(`DROP TYPE "appdata"."batch_batchstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."location"`);
        await queryRunner.query(`DROP TYPE "appdata"."location_locationstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."custodian"`);
        await queryRunner.query(`DROP TYPE "appdata"."custodian_custodianstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."productconfig"`);
        await queryRunner.query(`DROP TYPE "appdata"."productconfig_productconfigstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."product"`);
        await queryRunner.query(`DROP TYPE "appdata"."product_productstatus_enum"`);
        await queryRunner.query(`DROP INDEX "appdata"."IDX_a61c4df6102ad981264f5487ec"`);
        await queryRunner.query(`DROP TABLE "appdata"."sensor"`);
        await queryRunner.query(`DROP TYPE "appdata"."sensor_sensorstatus_enum"`);
        await queryRunner.query(`DROP TABLE "appdata"."user"`);
        await queryRunner.query(`DROP TYPE "appdata"."user_userstatus_enum"`);
        await queryRunner.query(`DROP TABLE "systemmaster"."group"`);
        await queryRunner.query(`DROP TABLE "systemmaster"."permission"`);
    }

}
