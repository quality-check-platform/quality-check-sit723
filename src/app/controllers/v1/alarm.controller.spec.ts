// std
import { ok, strictEqual } from 'assert';

// 3p
import { Context, createController, getHttpMethod, getPath, isHttpResponseOK } from '@foal/core';

// App
import { AlarmController } from './alarm.controller';

describe('AlarmController', () => {

  let controller: AlarmController;

  beforeEach(() => controller = createController(AlarmController));

  describe('has a "foo" method that', () => {

    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(AlarmController, 'foo'), 'GET');
      strictEqual(getPath(AlarmController, 'foo'), '/');
    });

    it('should return an HttpResponseOK.', () => {
      const ctx = new Context({});
      ok(isHttpResponseOK(controller.foo(ctx)));
    });

  });

});
