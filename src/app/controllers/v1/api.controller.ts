import { Context, Get, HttpResponseOK, UseSessions, UserRequired  } from '@foal/core';
import { fetchUser } from '@foal/typeorm';
import { User } from '../../entities';

@UseSessions({
	required: true,
	user: fetchUser(User)
  })
  @UserRequired()
  export class ApiController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK("API Controller");
  }

}
