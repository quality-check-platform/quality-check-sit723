import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Product, User } from '../../entities';


export class ProductController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK();
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getProduct(ctx: Context<User>, { id }: { id: string }) {
	const product = await Product.findOne({productid: id });
	if (!product) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(product);
  }

}
