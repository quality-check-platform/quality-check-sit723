import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Location, User } from '../../entities';


export class LocationController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK();
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getLocation(ctx: Context<User>, { id }: { id: string }) {
	const location = await Location.findOne({locationid: id });
	if (!location) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(location);
  }

}
