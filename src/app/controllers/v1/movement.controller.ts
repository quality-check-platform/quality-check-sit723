import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam, dependency } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Movement, User, Custodian, Location, Batch } from '../../entities';
import { Productquality } from '../../services';

export class MovementController {

	@dependency
	productquality: Productquality

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK();
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getMovement(ctx: Context<User>, { id }: { id: string }) {
	const movement = await Movement.findOne({movementid: id });
	if (!movement) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(movement);
  }

  	// Create a new Movement
	@Post('/')
	@ValidateBody({
	additionalProperties: false,
	properties: {
		movementtz: {type: 'string', format: 'date-time', default: Date()},
		movementtype: {type: 'string', default: "movement"},
		barcode: {type: 'string', default: ""},
		batchid: {type: 'string', format: 'uuid'},
		custodianid: {type: 'string', format: 'uuid'},
		locationid: {type: 'string', format: 'uuid'},
		catchstatus: {type: 'string', default: 'live'}		
	},
	required: ['custodianid', 'locationid'],
	type: 'object',
	})
	//@PermissionRequired('CreateBatch') // TODO
	async createMovement(ctx: Context<User>) {
	    const body = ctx.request.body;
		const movementtz = body.movementtz;
		const barcode = body.barcode; // (body.descr) ? body.descr.trim() : null;
		const movementtype = body.movementtype;
		const batchid = body.batchid;
		const locationid = body.locationid;
		const fk_custodian = body.custodianid;
		const catchstatus = body.catchstatus;

		if(!batchid && barcode == "") {
			return new HttpResponseBadRequest(`barcode or batchid not supplied.`);
		}

		let custodian;
		if(fk_custodian != undefined) {custodian = await Custodian.findOne({ custodianid: fk_custodian });}
		if(!custodian) {
			console.error(`Custodian ${fk_custodian} does not exist`);
			return new HttpResponseBadRequest(`Custodian "${fk_custodian}" does not exist`);
		}

		let batch;
		if(batchid != undefined) {batch = await Batch.findOne({ batchid });}
		else {batch = await Batch.findOne({ barcode });}
		if(!batch) {
			console.error(`Batch ${batchid} does not exist`);
			return new HttpResponseBadRequest(`Batch "${batchid}" | "${barcode}" does not exist`);
		}	

		let location;
		if(locationid != undefined) {location = await Location.findOne({ locationid });}
		if(!location) {
			console.error(`Location ${locationid} does not exist`);
			return new HttpResponseBadRequest(`Location "${locationid}" does not exist`);
		}	

		batch.fk_location = location; // update current batch location
		batch.catchstatus = catchstatus;

		let movement = new Movement();

		if(movementtz != undefined) {movement.movementtz = movementtz;} // otherwise defaults to now()
		movement.fk_batch = batch;
		movement.fk_custodian = custodian;
		movement.fk_location = location;
		movement.movementtype = movementtype;
		movement.catchstatus = catchstatus;
		try {
			const movementsaved = await movement.save();
		  console.log("movementsaved: " + movementsaved.movementid);
		  const savedbatch = await batch.save();

		  await this.productquality.updatebatchquality(batch.batchid);// TODO: calculate remaining shelf life based of product quality model
  
		  return new HttpResponseOK(`New movement created: ${movementsaved.movementid} and batch updated: ${savedbatch.batchid}`);
		} catch (error) {
		  console.error(error);
		  return new HttpResponseInternalServerError("An unknown error occured");
		}
	} // createProduct

}
