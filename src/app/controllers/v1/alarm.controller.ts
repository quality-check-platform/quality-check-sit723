import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Alarm, User } from '../../entities';

export class AlarmController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK("OK");
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getAlarm(ctx: Context<User>, { id }: { id: string }) {
	const alarm = await Alarm.find({ where: { batchid: id } });
	if (!alarm) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(alarm);
  }

  
}
