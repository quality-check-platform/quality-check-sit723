import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Batch, Product, Custodian, User, Movement, Location, Productconfig } from '../../entities';

export class BatchController {

	@Get('/')
	foo(ctx: Context) {
		return new HttpResponseOK("Product Controller");
	}

	@Get('/:id')
	@ValidatePathParam('id', { type: 'string', format: 'uuid' })
	async getBatch(ctx: Context<User>, { id }: { id: string }) {
		let resultset: any[] = [];
		// get the registration & movements
		const movements = await Movement.find({ where: { fk_batch : {batchid: id }}, relations: ["fk_batch", "fk_batch.fk_product", "alarms", "fk_custodian", "fk_location"], order: { movementtz:  "ASC"}});
		if (!movements) {
			return new HttpResponseNotFound();
		  }
		  resultset.push(movements);
		  // get the remainder of the movements
		  const batch = await Batch.findOne({ batchid: id }, { relations: ["fk_custodian", "fk_product"]});
		  if (!batch) {
			  return new HttpResponseNotFound();
			}
			resultset.push(batch);

			
		return new HttpResponseOK(resultset);
	}
  
	// Create a new Product Batch
	@Post('/')
	@ValidateBody({
	additionalProperties: false,
	properties: {
		batchcreatedtz: {type: 'string', format: 'date-time'},
		barcode: {type: 'string'},
		productid: {type: 'string', format: 'uuid'},
		custodianid: {type: 'string', format: 'uuid'},
		locationid: {type: 'string', format: 'uuid'},
		catchstatus: {type: 'string', default: 'live'}
	},
	required: ['productid', 'custodianid', 'locationid'],
	type: 'object',
	})
	//@PermissionRequired('CreateBatch') // TODO
	async createProduct(ctx: Context<User>) {
	    const body = ctx.request.body;
		
		const batchcreatedtz = body.batchcreatedtz;
		const barcode = body.barcode; // (body.descr) ? body.descr.trim() : null;
		const productid = body.productid;
		const locationid = body.locationid;
		const fk_custodian = body.custodianid;
		const catchstatus = body.catchstatus;

		let custodian;
		if(fk_custodian != undefined) {custodian = await Custodian.findOne({ custodianid: fk_custodian });}
		if(!custodian) {
			console.error(`Custodian ${fk_custodian} does not exist`);
			return new HttpResponseBadRequest(`Custodian "${fk_custodian}" does not exist`);
		}

		let product;
		if(productid != undefined) {product = await Product.findOne({ productid });}
		if(!product) {
			console.error(`Product ${productid} does not exist`);
			return new HttpResponseBadRequest(`Product "${productid}" does not exist`);
		}

		const productconfig = await Productconfig.findOne({where: {fk_product : product, catchstatus : catchstatus}});
		if(!productconfig) {
			console.error(`Product ${productid} does not have any config`);
		}
		console.log(productconfig);


		let location;
		if(locationid != undefined) {location = await Location.findOne({ locationid });}
		if(!location) {
			console.error(`Location ${locationid} does not exist`);
			return new HttpResponseBadRequest(`Location "${locationid}" does not exist`);
		}	

		let batch = new Batch();
		// if we want to allow updating of an existing batch...
		//let batch;
		//if(!batchid){ batch = await Batch.findOne({ batchid: batchid })}
		//else { batch = new Batch(); }

		if(batchcreatedtz != undefined) {batch.batchcreatedtz = batchcreatedtz;} // otherwise defaults to now()
		batch.fk_product = product;
		batch.fk_custodian = custodian;
		batch.fk_location = location;
		batch.catchstatus = catchstatus;
		if(barcode != undefined) {batch.barcode = barcode;}
		if (productconfig) {
			batch.shelflife = productconfig.shelflife; // copy in the default shelf life for this product model based on catch status (live | raw | cooked)
		}
		try {
			const batchsaved = await batch.save();
		  console.log("batchsaved: " + batchsaved.batchid);
		  if(barcode == undefined || barcode == "")
		  {
			  batchsaved.barcode = batchsaved.batchid;
			  console.log(
				  await batchsaved.save()
			  );
		  }
		  // create a registration movement to seed the batch
		  let movement = new Movement();
		  movement.fk_batch = batchsaved;
		  movement.fk_custodian = custodian;
		  movement.fk_location = location;
		  movement.movementtz = batchcreatedtz;
		  movement.movementtype = 'registration';
		  movement.catchstatus = catchstatus;
		  await movement.save();
  
		  return new HttpResponseOK(`New batch created: ${batchsaved.batchid}`);
		} catch (error:any) {
			console.error(error);
			if(error.code == 23505)
			{
				return new HttpResponseBadRequest(`Batch already exists: ${barcode}`);
			}
		  return new HttpResponseInternalServerError("An unknown error occured");
		}	

	} // createProduct

} // class