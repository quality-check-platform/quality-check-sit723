import { Context, controller, Get, HttpResponseOK } from '@foal/core';
import { SocialController, TokenController } from './auth';

export class AuthController {
  subControllers = [
    controller('/token', TokenController),
    controller('/social', SocialController)
  ];


  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK("Auth Controller");
  }

}
