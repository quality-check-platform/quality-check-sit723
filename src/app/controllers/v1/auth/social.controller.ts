import { Context, dependency, Get, Post, HttpResponseOK, HttpResponseRedirect, HttpResponseNoContent, Session, createSession, Store } from '@foal/core';
import { GoogleProvider } from '@foal/social';
import { User } from '../../../entities';
import { Disk } from '@foal/storage';

interface GoogleUserInfo {
  email: string;
  name?: string;
}

export class SocialController {
  @dependency
  google: GoogleProvider;

  @dependency
  store: Store;

  @Get('/google')
  redirectToGoogle() {
    return this.google.redirect();
  }

  @Get('/google/callback')
  async handleGoogleRedirection(ctx: Context<User, Session>) {
    const { userInfo } = await this.google.getUserInfo<GoogleUserInfo>(ctx);

    if (!userInfo.email) {
      throw new Error('Google should have returned an email address.');
    }

    let user = await User.findOne({ email: userInfo.email });

    if (!user) {
      user = new User();
      user.email = userInfo.email;
      user.name = userInfo.name ?? 'Unknown';

      await user.save();
    }

	ctx.session = await createSession(this.store);

    ctx.session.setUser(user);
    ctx.user = user;

    return new HttpResponseRedirect('/');
  }

  @Post('/logout')
  async logout(ctx: Context<User|undefined, Session>) {
    await ctx.session.destroy();
    return new HttpResponseNoContent();
  }

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK("social");
  }
}
