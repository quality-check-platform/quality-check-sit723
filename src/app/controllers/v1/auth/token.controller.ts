import { Context, Get, dependency, Store, createSession, HttpResponseNoContent, HttpResponseOK, HttpResponseUnauthorized, Post, Session, ValidateBody, verifyPassword } from '@foal/core';
import { User } from '../../../entities';

const credentialsSchema = {
  type: 'object',
  properties: {
    email: { type: 'string', format: 'email', maxLength: 255 },
    password: { type: 'string' }
  },
  required: [ 'email', 'password' ],
  additionalProperties: false,
};

export class TokenController {
	@dependency
	store: Store;
  
	@Get('/')
	foo(ctx: Context) {
	  return new HttpResponseOK("Token Controller");
	}
  
  @Post('/login')
  @ValidateBody(credentialsSchema)
  async login(ctx: Context<User|undefined, Session>) {
    const email = ctx.request.body.email;
    const password = ctx.request.body.password;

    const user = await User.findOne({ email });
    if (!user) {
      return new HttpResponseUnauthorized();
    }

    if (!(await verifyPassword(password, user.password))) {
      return new HttpResponseUnauthorized();
    }

	if (!await verifyPassword(password, user.password)) {
		return new HttpResponseUnauthorized();
	  }
  
	  ctx.session = await createSession(this.store);
	  ctx.session.setUser(user);
  
	  return new HttpResponseOK({
		token: ctx.session.getToken()
	  });
  }

  @Post('/logout')
  async postlogout(ctx: Context<User|undefined, Session>) {
    await ctx.session.destroy();
    return new HttpResponseNoContent();
  }

  @Get('/logout')
  async getlogout(ctx: Context<User|undefined, Session>) {
    await ctx.session.destroy();
    return new HttpResponseNoContent();
  }

}
