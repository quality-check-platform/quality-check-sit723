import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Sensor, User } from '../../entities';

export class SensorController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK();
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getSensor(ctx: Context<User>, { id }: { id: string }) {
	const sensor = await Sensor.findOne({sensorid: id });
	if (!sensor) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(sensor);
  }

}
