// std
import { ok, strictEqual } from 'assert';

// 3p
import { Context, createController, getHttpMethod, getPath, isHttpResponseOK } from '@foal/core';

// App
import { CustodianController } from './custodian.controller';

describe('CustodianController', () => {

  let controller: CustodianController;

  beforeEach(() => controller = createController(CustodianController));

  describe('has a "foo" method that', () => {

    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(CustodianController, 'foo'), 'GET');
      strictEqual(getPath(CustodianController, 'foo'), '/');
    });

    it('should return an HttpResponseOK.', () => {
      const ctx = new Context({});
      ok(isHttpResponseOK(controller.foo(ctx)));
    });

  });

});
