import { Context, Get, Post, HttpResponseOK, ValidatePathParam, ValidateBody, HttpResponseNotFound, dependency, Config } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Sensor, Sensortype, Sensortype_lnk_eventtype, Batch, User, Location, Productconfig } from '../../entities';
import jp = require('jsonpath');
import { Datatransformers } from '../../services';
import { Productquality } from '../../services';
const { InfluxDB, Point } = require('@influxdata/influxdb-client')

export class EventController {

	@dependency
	datatransformer: Datatransformers
	@dependency
	productquality: Productquality


	@Get('/')
	foo(ctx: Context) {
		return new HttpResponseOK("Event Controller");
	}

	@Post('/:id')
	//@PermissionRequired('CreateBatch') // TODO
	@ValidatePathParam('id', { type: 'string', format: 'uuid' })
	async postEvent(ctx: Context<User>, { id }: { id: string }) {
		const body = ctx.request.body;
		const sensortype = await Sensortype.find({ where: { sensortypeid: id }, relations: ["sensortype_lnk_eventtype", "sensortype_lnk_eventtype.fk_eventtype"] });
		if (!sensortype) {
			return new HttpResponseNotFound();
		}
		// TODO: check for undefined / blank value
		const sensorid = "" + jp.query(body, sensortype[0].sensoridpath);
		// use sensorid to find the batch number
		console.log("Getting batches by sensor id " + sensorid);
		const batch = await Sensor.find({ where: { sensorexternalid: sensorid }, relations: ["fk_location", "fk_location.batch", "fk_location.batch.fk_product", "fk_location.batch.fk_product.fk_productconfig"] });
		// loop through sensortype_lnk_eventtype values to build events, add to influxdb
		console.log("batch");
		console.log(batch);
		// if ...batch[] is empty then there are no batches at the location of this sensor.
		const batches = jp.query(batch, "$..fk_location.batch")[0];
		console.log(batches);
		if (!batches) {
			console.error("No batches located at this sensor. dropping data " + sensorid);
			return new HttpResponseOK("No batches located at this sensor.");
		}

		// now create events and insert
		let numevents = 0;
		const eventtypes = jp.query(sensortype, "$..sensortype_lnk_eventtype")[0];
		if (eventtypes.length == 0) {
			console.log("No event types match this sensor. dropping data");
			return new HttpResponseOK("No event types match this sensor.");
		}

		const token = Config.getOrThrow('influxdb.influxdb_token', 'string');
		const org = Config.getOrThrow('influxdb.influxdb_org', 'string');
		const bucket = Config.getOrThrow('influxdb.influxdb_bucket', 'string');
		const url = Config.getOrThrow('influxdb.influxdb_url', 'string');
		const client = new InfluxDB({ url: url, token: token })
		const writeApi = client.getWriteApi(org, bucket)

		for (const cbatch of batches) {
			// check if there are productconfig quality thresholds to check for

			let productconfig = await Productconfig.findOne({ where: { fk_product: cbatch.fk_product, catchstatus: cbatch.catchstatus }});
			if(!productconfig) {
				console.log("No product config for this product: " + cbatch.fk_product.productid);
			}
			else {
				console.log(productconfig);
			}

			for (const eventtype of eventtypes) {
				console.log("Event for batch: " + cbatch.batchid + " for event type: " + eventtype.sensoreventtypename);
				// insert
				let datamap = eventtype.datamap;
				let jdatamap = JSON.parse("" + datamap);
				let point = new Point('event');
				for (let i = 0; i < jdatamap.length; i++) {
					if(jdatamap[i][0] != "timestamp") {
						point.floatField(jdatamap[i][0], this.datatransformer.applytransform(jdatamap[i][2], "" + jp.query(body, jdatamap[i][1])));
						// check simple models for alarms to be raised
						if(productconfig) {
							if(productconfig.eventtypename == eventtype.fk_eventtype.eventtypename && productconfig.alarmkey == jdatamap[i][0] && productconfig.alarmthreshold < parseInt(this.datatransformer.applytransform(jdatamap[i][2], "" + jp.query(body, jdatamap[i][1])))) {
								console.log("Have alarm for batchid " + cbatch.batchid);
								let alarmresult = await this.productquality.logalarm(cbatch.batchid, productconfig.alarmkey, parseInt(this.datatransformer.applytransform(jdatamap[i][2], "" + jp.query(body, jdatamap[i][1]))));
								if(alarmresult){
									console.log("Alarm saved successfully");
								}
							}
						} // productconfig
					}
					else {
						point.timestamp((new Date(this.datatransformer.applytransform(jdatamap[i][2], "" + jp.query(body, jdatamap[i][1])))).getTime()*1000000);
					} // ! timestamp
				}
				point.tag('batchid', cbatch.batchid);
				point.tag('datatype', eventtype.fk_eventtype.eventtypename);
				point.precision = "s";
				point.tag('sensorname', eventtype.sensoreventtypename);
				writeApi.writePoint(point)
				console.log("new point created: ");
				console.log(point);

				numevents++;
			} // for event type
		} // for batch
		try {
			await writeApi.close();
			console.log('Finished write to influxdb');
		}

		catch (e) {
			console.log('\\ERROR writing tio influxdb:');
			console.error(e)
		}
		return new HttpResponseOK(numevents + " event" + ((numevents > 1) ? "s" : "") + " logged for " + batches.length + " batch" + ((batches.length > 1) ? "es" : ""));
	}
}
