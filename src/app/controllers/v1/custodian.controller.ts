import { Context, Get, HttpResponseBadRequest, HttpResponseInternalServerError, HttpResponseOK, HttpResponseNotFound, Patch, Post, ValidateBody, ValidatePathParam } from '@foal/core';
import { PermissionRequired } from '@foal/typeorm';
import { Custodian, User } from '../../entities';

export class CustodianController {

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK();
  }

  @Get('/:id')
  @ValidatePathParam('id', { type: 'string', format: 'uuid' })
  async getCustodian(ctx: Context<User>, { id }: { id: string }) {
	const custodian = await Custodian.findOne({custodianid: id });
	if (!custodian) {
		return new HttpResponseNotFound();
	  }
	  return new HttpResponseOK(custodian);
  }

}
