import { SwaggerController } from '@foal/swagger';
import { V1Controller } from './v1.controller';

export class OpenapiController extends SwaggerController  {

  options = {
    controllerClass: V1Controller
  }

}
