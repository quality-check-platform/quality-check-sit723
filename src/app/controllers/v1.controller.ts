import { ApiInfo, ApiServer, Context, controller, Get, HttpResponseOK } from '@foal/core';
import { AlarmController, ApiController, AuthController, BatchController, CustodianController, EventController, LocationController, MovementController, ProductController, SensorController } from './v1';

@ApiInfo({
	title: 'Quality Check Platform API',
	version: '1.0.0'
  })
  @ApiServer({
	url: '/v1'
  })

export class V1Controller {
  subControllers = [
    controller('/event', EventController),
    controller('/batch', BatchController),
    controller('/auth', AuthController),
    controller('/api', ApiController),
    controller('/location', LocationController),
    controller('/sensor', SensorController),
    controller('/movement', MovementController),
    controller('/custodian', CustodianController),
    controller('/alarm', AlarmController),
    controller('/product', ProductController),
  ];

  @Get('/')
  foo(ctx: Context) {
    return new HttpResponseOK("Quality Check Platform API V1");
  }

}
