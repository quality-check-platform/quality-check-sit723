// std
import { ok, strictEqual } from 'assert';

// 3p
import { Context, createController, getHttpMethod, getPath, isHttpResponseOK } from '@foal/core';

// App
import { V1Controller } from './v1.controller';

describe('V1Controller', () => {

  let controller: V1Controller;

  beforeEach(() => controller = createController(V1Controller));

  describe('has a "foo" method that', () => {

    it('should handle requests at GET /.', () => {
      strictEqual(getHttpMethod(V1Controller, 'foo'), 'GET');
      strictEqual(getPath(V1Controller, 'foo'), '/');
    });

    it('should return an HttpResponseOK.', () => {
      const ctx = new Context({});
      ok(isHttpResponseOK(controller.foo(ctx)));
    });

  });

});
