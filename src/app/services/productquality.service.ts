import { Alarm, Movement } from '../entities';

export class Productquality {

	updatebatchquality(batchid:string): boolean {
		console.log("updatebatchquality: " + batchid);

		return true;
	}

	async logalarm(batchid:string, alarmkey:string, alarmvalue:number): Promise<boolean> {
		const movement = await Movement.findOne({ where: {fk_batch: {batchid : batchid}}, relations: ["fk_batch"] });
		if(!movement) {
			console.error("Batch " + batchid + " does not exist");
			return false;
		}	

		let alarm = new Alarm();
		alarm.alarmvalue = alarmvalue;
		alarm.batchid = movement.fk_batch;
		alarm.movementid = movement;
		alarm.eventtypename = alarmkey;
		try {
			let savedalarm = await alarm.save();
		} catch (error) {
			console.error(error);
		}
		return true;
	}
}
