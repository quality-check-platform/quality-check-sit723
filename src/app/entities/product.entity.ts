// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Productconfig } from './productconfig.entity';
@Entity({
	schema: "appdata"
})
export class Product extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	productid: string;

	@Column({ type: "citext" })
	productname: string;

	@Column({ type: "citext" })
	productontology: string;

	@OneToMany(() => Productconfig, fk_productconfig => fk_productconfig.fk_product)
	fk_productconfig: Productconfig[];

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	productstatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
