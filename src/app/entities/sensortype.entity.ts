// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, JoinTable, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Sensortype_lnk_eventtype } from './sensortype_lnk_eventtype.entity';

@Entity({
	schema: "appdata"
	// ,orderBy: { id: "ASC"}
})
@Index(["sensortypename"], { unique: true })
export class Sensortype extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	sensortypeid: string;

	// common fields that all events should have
	@Column({ type: "citext" }) // manufacturer - model
	sensortypename: string;

	@Column({ type: "citext" }) // format of payload: json, xml, hex, custom...
	datatype: string;

	@Column({ type: "citext" }) // does the payload contain single sensor data or multi sensor data
	singularity: string; //deprecated. present for backwards compatibility only

	@Column({ type: "citext" }) // path selector for the sensor ID
	sensoridpath: string;

	@OneToMany(() => Sensortype_lnk_eventtype, sensortype_lnk_eventtype => sensortype_lnk_eventtype.fk_sensortype)
	sensortype_lnk_eventtype: Sensortype_lnk_eventtype[];

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	sensortypeStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
