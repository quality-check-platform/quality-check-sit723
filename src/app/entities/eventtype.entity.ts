// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, JoinTable, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Sensortype_lnk_eventtype } from './sensortype_lnk_eventtype.entity';

@Entity({
	schema: "appdata"
})
export class Eventtype extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	eventtypeid: string;

  @Column({ type: "citext" }) // temperature, location, etc
  eventtypename: string;

  @OneToMany(() => Sensortype_lnk_eventtype, sensortype_lnk_eventtype => sensortype_lnk_eventtype.fk_sensortype)
  sensortype_lnk_eventtype: Sensortype_lnk_eventtype[];

  @Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	eventtypeStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
