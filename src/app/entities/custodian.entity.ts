// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';

@Entity({
	schema: "appdata"
})
export class Custodian extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	custodianid: string;

	@Column({ type: "citext" })
	custodianname: string;

	@Column({ type: "citext", nullable: true })
	registrationnumber: string;

	@Column({ type: "citext", nullable: true })
	street1: string;

	@Column({ type: "citext", nullable: true })
	street2: string;

	@Column({ type: "citext", nullable: true })
	city: string;

	@Column({ type: "citext", nullable: true })
	state: string;

	@Column({ type: "citext", nullable: true })
	postcode: string;

	@Column({ type: "citext", nullable: true })
	country: string;

	@Column({ type: "citext", nullable: true })
	companycompliance: string;

	@Column({ type: "citext", nullable: true })
	companyurl: string;

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	custodianstatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
