
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Custodian } from './custodian.entity';
import { Sensor } from './sensor.entity';
import { Batch } from './batch.entity';
import * as datatypes from './datatypes';

@Entity({
	schema: "appdata"
})
export class Location extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	locationid: string;

	@Column({ type: "citext" })
	locationname: string;

	@Column({ type: "citext" })
	locationtype: string; // fixed | mobile

	@Column({ nullable: true })
	lat: number; // TODO: upgrade this to PostGIS

	@Column({ nullable: true })
	long: number;

	@Column({ type: "citext", nullable: true })
	city: string;

	@Column({ type: "citext", nullable: true })
	state: string;

	@Column({ type: "citext", nullable: true })
	postcode: string;

	@Column({ type: "citext", nullable: true })
	country: string;

	@ManyToOne(() => Custodian, { nullable: false })
	fk_custodian: Custodian;
	
	@OneToMany(() => Batch, batch => batch.fk_location)
	batch: Batch[];

	@OneToMany(() => Sensor, sensors => sensors.fk_location)
	sensors: Sensor[];

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	locationstatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
