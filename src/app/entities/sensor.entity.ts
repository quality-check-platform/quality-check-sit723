// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Location, Sensortype } from '.';
@Entity({
	schema: "appdata"
})
@Index(["sensorexternalid"], { unique: true })
export class Sensor extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	sensorid: string;

	@Column({ type: "citext" })
	sensorexternalid: string;

	@ManyToOne(() => Location )
	fk_location: Location;

	@ManyToOne(() => Sensortype )
	fk_sensortype: Sensortype;

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	sensorStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
