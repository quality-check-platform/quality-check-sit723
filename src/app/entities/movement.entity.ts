// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Custodian } from './custodian.entity';
import { Location } from './location.entity';
import * as datatypes from './datatypes';
import { Batch } from './batch.entity';
import { Alarm } from './alarm.entity';

@Entity({
	schema: "appdata"
})
export class Movement extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	movementid: string;

	@Column({ type: "citext" }) // registration | handover | internal TODO: change to enum for performance
	movementtype: string;

	@Column({ type: "timestamptz" })
	movementtz: Date;

	@Column({ type: "citext", nullable: true  })
	catchstatus: string; // raw | live | cooked // TODO: make enum

	// FK: Custodian
	@ManyToOne(() => Custodian, { nullable: false })
	fk_custodian: Custodian;

	// FK: Location
	@ManyToOne(() => Location, { nullable: false })
	fk_location: Location;

	// FK: Product Batch
	@ManyToOne(() => Batch, { nullable: false })
	fk_batch: Batch;

	// Alarms
	@OneToMany(() => Alarm, alarms => alarms.movementid)
	alarms: Alarm[];

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	movementstatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
