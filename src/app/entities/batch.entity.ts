// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Custodian } from './custodian.entity';
import { Product } from './product.entity';
import { Location } from "./location.entity";
import * as datatypes from './datatypes';
import { Alarm } from '.';

@Entity({
	schema: "appdata"
})
@Index(["barcode"], { unique: true })
export class Batch extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	batchid: string;

	@Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
	batchcreatedtz : Date;

	// FK: Batch: Product
	@ManyToOne(() => Product, { nullable: false })
	fk_product: Product;

	// FK: Batch: Custodian
	@ManyToOne(() => Custodian, { nullable: false })
	fk_custodian: Custodian;

	@Column({ type: "citext", nullable: true  })
	barcode: string; // Barcode, RFID, QR Code etc. for QR Code just use the UUID.

	@ManyToOne(() => Location, { nullable: false })
	fk_location: Location;

	@Column({ type: "citext", nullable: true  }) // current status as at last movement
	catchstatus: string; // raw | live | cooked // TODO: make enum

	// Quality data
	@Column({ nullable: true, default: 0 }) // hours above alarm threshold
	alarmqty: number;

	@Column({ nullable: true, default: 0 }) // hour - degrees above threshold
	alarmvolume: number;

	@Column({ nullable: true }) // hours remainig in shelf life
	shelflife: number;

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	batchstatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
