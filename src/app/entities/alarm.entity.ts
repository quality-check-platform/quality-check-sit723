// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Batch, Movement } from '.';
@Entity({
	schema: "appdata"
})
export class Alarm extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	alarmid: string;

	@Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
	alarmtz : Date;

	@ManyToOne(() => Batch, { nullable: false })
	batchid: Batch;

	// link the alarm back ot the Location to associate location, custodian
	@ManyToOne(() => Movement, { nullable: false })
	movementid: Movement;

	@Column({ type: "citext" }) // temperature, location, etc
	eventtypename: string;
  
	@Column({ nullable: true })
	alarmvalue: number;
  
	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	alarmStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
