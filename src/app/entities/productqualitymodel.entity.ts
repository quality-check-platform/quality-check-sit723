// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Product } from './product.entity';

@Entity({
	schema: "appdata"
	// ,orderBy: { id: "ASC"}
})
@Index(["catchstatus", "eventtypename", "fk_product", "alarmpoint"], { unique: true })
export class Productqualitymodel extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	productqualitymodelid: string;

	@Column({ type: "citext", nullable: true  })
	catchstatus: string; // raw | live | cooked // TODO: make enum

	// TODO: make this abstracted so that complex data can trigger shelf life reduction rates
	@Column({ type: "citext" }) // what event type is linked to this alarm state: temperature, location, etc
	eventtypename: string;
	
	// TODO: make this abstracted so that complex data can trigger shelf life reduction rates
	@Column({ nullable: true })
	alarmconfig: string;

	// interim model - use a single value for model config
	@Column({ type: "decimal", nullable: true }) // temperature for the reduction rate
	alarmpoint: number;

	@Column({ nullable: true }) // reduction of shelf life per hour at this temp
	shelflifereductionrate: number;
	// FK: Batch: Product
	@ManyToOne(() => Product, { nullable: false })
	fk_product: Product;
	
  @Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	productconfigStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;


}
