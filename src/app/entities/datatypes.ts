export enum sysStatusenum {
    Current = 'Current',
    Archived = 'Archived',
    Draft = 'Draft'
}