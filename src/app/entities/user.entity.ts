// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { UserWithPermissions } from '@foal/typeorm';
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
// import { hashPassword } from '@foal/core';
import { Custodian } from '.';

@Entity({
	schema: "appdata"
})
export class User extends UserWithPermissions {

	@PrimaryGeneratedColumn()
	id: number;
  
	@Column({ unique: true, type: "citext" })
	email: string;

	@Column({ nullable: true })
  	password: string;

	@Column({ type: "citext" })
	name: string;
 
		// FK: Custodian
		@ManyToOne(() => Custodian, { nullable: false })
		fk_custodian: Custodian;
	
	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	userStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}

export { Group, Permission } from '@foal/typeorm';
