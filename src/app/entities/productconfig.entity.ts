// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Product } from './product.entity';

@Entity({
	schema: "appdata"
})
@Index(["fk_product", "catchstatus"], { unique: true })
export class Productconfig extends BaseEntity {

	@PrimaryGeneratedColumn("uuid")
	productconfigid: string;

	// FK: Batch: Product
	@ManyToOne(() => Product, fk_product => fk_product.fk_productconfig, { nullable: false })
	fk_product: Product;
	
	@Column({ type: "citext"  })
	catchstatus: string; // raw | live | cooked // TODO: make enum

	// TODO: make this abstracted so that multiple events could trigger alarms
	@Column({ type: "citext" }) // what event type is linked to this alarm state: temperature, location, etc
	eventtypename: string;
	
	// TODO: make this abstracted so that multiple events could trigger alarms
	@Column({ nullable: true })
	alarmkey: string;
	@Column({ nullable: true })
	alarmthreshold: number;

	@Column({ nullable: true }) // remaining shelf life in hours (live "best before")
	shelflife: number;
	
	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	productconfigStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
