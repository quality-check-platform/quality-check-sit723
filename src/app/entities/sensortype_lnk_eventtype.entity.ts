// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, ManyToMany, JoinTable, Index, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import * as datatypes from './datatypes';
import { Sensortype, Eventtype } from '.';

@Entity({
	schema: "appdata"
})
@Index(["sensoreventtypename"], { unique: true })
export class Sensortype_lnk_eventtype extends BaseEntity {

	@PrimaryGeneratedColumn()
	id: number;

	@Column({ type: "citext" }) // temperature, location, etc concat of sensortype and eventtype. here for ease of updating data in scripts
	sensoreventtypename: string;

	@Column({ type: "citext" }) // array of paceholder:select path keys mapped to eventtype:eventtypestructure
	datamap: string;

	@ManyToOne(type => Sensortype, { nullable: false })
	fk_sensortype: Sensortype;

	@ManyToOne(type => Eventtype, { nullable: false})
	fk_eventtype: Eventtype;

	@Column({
	type: 'enum',
	enum : datatypes.sysStatusenum,
	default: datatypes.sysStatusenum.Current
	})
	lnkStatus: datatypes.sysStatusenum;

	@CreateDateColumn({ type: 'timestamptz' })
	createdAt : Date;

	@UpdateDateColumn({ type: 'timestamptz' })
	lastUpdated : Date;

	@DeleteDateColumn({ type: 'timestamptz' })
	deletedAt : Date;

	@VersionColumn()
	entityVersion : number;

}
