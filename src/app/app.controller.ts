import { Context, controller, Get, IAppController, HttpResponseNotFound } from '@foal/core';
import { createConnection } from 'typeorm';

import { OpenapiController, V1Controller } from './controllers';

export class AppController implements IAppController {
  subControllers = [
    controller('/v1', V1Controller),
    controller('/openapi', OpenapiController)
  ];

  async init() {
    await createConnection();
  }

  @Get('*')
  renderApp(ctx: Context) {
      return new HttpResponseNotFound(); // 404 for anything not explicitly defined.
  }
}
