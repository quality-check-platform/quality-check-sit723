/*
foal run create-custodian custodianname="Company Name"
*/

// 3p
import { createConnection } from 'typeorm';
import { Custodian } from '../app/entities';

export const schema = {
  additionalProperties: false,
  properties: {
	custodianname: {type: 'string'},
	registrationnumber: {type: 'string'},
	street1: {type: 'string'},
	street2: {type: 'string'},
	city: {type: 'string'},
	state: {type: 'string'},
	postcode: {type: 'string'},
	country: {type: 'string', default: "Australia"},
	companycompliance: {type: 'string'},
	companyurl: {type: 'string'},
  },
  required: [ 'custodianname' ],
  type: 'object',
};

export async function main(args: any) {
	const connection = await createConnection();

	const custodianname = args.custodianname;
	const registrationnumber = args.registrationnumber;
	const street1 = args.street1;
	const street2 = args.street2;
	const city = args.city;
	const state = args.state;
	const postcode = args.postcode;
	const country = args.country;
	const companycompliance = args.companycompliance;
	const companyurl = args.companyurl;
	let custodian = await Custodian.findOne({ custodianname });
	if(!custodian) {
		custodian = new Custodian();
	}
	custodian.custodianname = custodianname;
	custodian.registrationnumber = registrationnumber;
	custodian.street1 = street1;
	custodian.street2 = street2;
	custodian.city = city;
	custodian.state = state;
	custodian.postcode = postcode;
	custodian.country = country;
	custodian.companycompliance = companycompliance;
	custodian.companyurl = companyurl;

  try {
	console.log(
		await custodian.save()
	);
  } catch (error) {
    console.error(error);
  } finally {
    await connection.close();
  }
}
