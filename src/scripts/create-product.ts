/*
foal run create-product productname="Rock Lobster" productontology="Shellfish Food Product"
*/

// 3p
import { createConnection } from 'typeorm';
import { Product } from '../app/entities';

export const schema = {
  additionalProperties: false,
  properties: {
	productname: {type: 'string'},
	productontology: {type: 'string'}
  },
  required: [ 'productname' ],
  type: 'object',
};

export async function main(args: {productname: string, productontology: string}) {
  const connection = await createConnection();
  const productname = args.productname;
  const productontology = args.productontology;
  let product = await Product.findOne({ productname });
  if(!product) {
	product = new Product();
  }
  product.productname = productname;
  product.productontology = productontology;

try {
  console.log(
	  await product.save()
  );
  } catch (error) {
    console.error(error);
  } finally {
    await connection.close();
  }
}
