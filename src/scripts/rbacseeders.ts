// 3p
import { createConnection, getConnection } from 'typeorm';
import { Group, Permission } from '@foal/typeorm';
import { User } from '../app/entities';

export const schema = {
  additionalProperties: false,
  properties: {
  },
  required: [ ],
  type: 'object',
};

export async function main(args: any) {
  try {
	//createpermission(codeName, name);
	//await createpermission("", "");
	await createpermission("aaa", "A Thing");
	await createpermission("bbb", "B Thing");
	await createpermission("ccc", "C Thing");
	//creategroup(groupName, [permission, permission]);
	//await creategroup("", ["", ""]);
	await creategroup("admins", "Admins", ["aaa", "bbb"]);
	await creategroup("paidusers", "Paid Users", ["aaa", "bbb", "ccc"]);
	await creategroup("freeusers", "Free Users", ["aaa", "bbb"]);

	//await createuser(email: string, name: string, groups: string[])
	//await createuser("email", "name":, ["groupName", "groupName", "groupName"])
	await createuser("jarviswalker@gmail.com", "Jarvis Walker", ["admins", "paidusers"])

  } catch (error) {
    console.error(error);
  } finally {
   // await connection.close();
  }
}

async function createpermission(codeName: string, name: string)
{
	console.log("createpermission: " + codeName);
	const connection = await createConnection();

	const permission = await Permission.findOne({ codeName });
	if (permission) {
	  console.log(
		`Permission already exists: "${codeName}".`
	  );
	  await connection.close();
	  return;
	}

	const newpermission = new Permission();
	newpermission.codeName = codeName;
	newpermission.name = name;
  
	try {
	  console.log(
		  await newpermission.save()
	  );
	} catch (error) {
	  console.log(error.message);
	} finally {
		await connection.close();
	}
}

async function creategroup(codeName: string, groupname: string, permissions: string[])
{
	console.log("creategroup: " + codeName);
	const connection = await createConnection();
	const groupexists = await Group.findOne({ codeName });
	const group = (groupexists) ? groupexists : new Group();
	group.permissions = [];
	group.codeName = codeName;
	group.name = groupname;
  
	try {
	  for (const codeName of permissions) {
		const permission = await Permission.findOne({ codeName });
		if (!permission) {
		  console.log(
			`No permission with the code name "${codeName}" was found. Please create permissions before assigning them to groups.`
		  );
		  await connection.close();
		  return;
		}
		group.permissions.push(permission);
	  }
  
	  console.log(
		await group.save()
	  );
	} catch (error) {
	  console.log(error.message);
	} finally {
		await connection.close();
	}
}

async function createuser(email: string, name: string, groups: string[])
{
	console.log("createuser: " + email);
	const connection = await createConnection();
	const userexists = await User.findOne({ email });
	const user = (userexists) ? userexists : new User();
	user.email = email;
	user.name = name;
	user.groups = [];

	for (const codeName of groups as string[]) {
		const group = await Group.findOne({ codeName });
		if (!group) {
		  console.log(`No group with the code name "${codeName}" was found. Please create groups before assigning to users.`);
		  await connection.close();
		  return;
		}
		user.groups.push(group);
	  }
	  try {
		console.log(
		  await user.save()
		);
	} catch (error) {
		console.log(error.message);
	} finally {
		await connection.close();
	}
}