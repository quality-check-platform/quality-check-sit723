@echo off
rem call foal run rbacseeders
rem call foal run create-user email=jarviswalker@gmail.com groups="[\"Unlimited\",\"Admin\"]"

echo Create products
call foal run create-product productname="Rock Lobster" productontology="Shellfish Food Product"
call foal run create-product productname="Salmon" productontology="Fish Food Product"
call foal run create-product productname="Tuna" productontology="Fish Food Product"
call foal run create-product productname="Abalone" productontology="Shellfish Food Product"
call foal run create-product productname="Prawns" productontology="Shrimp Food Product"

call foal run create-product productname="Crabs" productontology="Shellfish Food Product"
call foal run create-product productname="Scallops" productontology="Shellfish Food Product"
call foal run create-product productname="Sea Bass" productontology="Fish Food Product"
call foal run create-product productname="Flathead" productontology="Fish Food Product"
call foal run create-product productname="Yellowtail Kingfish" productontology="Fish Food Product"
call foal run create-product productname="Ocean Jacket" productontology="Fish Food Product"
call foal run create-product productname="King George Whiting" productontology="Fish Food Product"


echo create Custodians

call foal run create-custodian custodianname="Petes Food Group" 
call foal run create-custodian custodianname="Calm Seas" 
call foal run create-custodian custodianname="Great Seafood" 
call foal run create-custodian custodianname="AJs Bubbles and Seafood bar" 
call foal run create-custodian custodianname="Frankies Fisheries" 
call foal run create-custodian custodianname="Parisian Logistics Ltd" 
call foal run create-custodian custodianname="Waelchi and Sons Cargo" 
call foal run create-custodian custodianname="Greenfelder" 
call foal run create-custodian custodianname="Carroll Group Seafoods" 
call foal run create-custodian custodianname="Downtown Oyster Bar" 
call foal run create-custodian custodianname="Willms Ltd" 
call foal run create-custodian custodianname="Collins-Treutel" 
call foal run create-custodian custodianname="Coles New World" 
call foal run create-custodian custodianname="Skiles Freight" 
call foal run create-custodian custodianname="Bobs Rocklobsters" 
call foal run create-custodian custodianname="Senger Cold Storage" 
call foal run create-custodian custodianname="Lennys Supermarket" 
call foal run create-custodian custodianname="Woolies" 
call foal run create-custodian custodianname="Weber Fishing" 
call foal run create-custodian custodianname="Secret Seas" 
call foal run create-custodian custodianname="ABC Cold Storage" 
call foal run create-custodian custodianname="National Seafoods" 
call foal run create-custodian custodianname="Melbourne Seafood Sales" 
call foal run create-custodian custodianname="Dodgy Fish" 
call foal run create-custodian custodianname="Low Grade Logistics" 
call foal run create-custodian custodianname="All Natural Seafood" 
call foal run create-custodian custodianname="Top Shelf Logistics" 
call foal run create-custodian custodianname="Simply Seafood Restaurant" 
call foal run create-custodian custodianname="Aus Fishies" 
call foal run create-custodian custodianname="Southern Seafood Distribution" 
call foal run create-custodian custodianname="Arctic Transport" 
call foal run create-custodian custodianname="Sea Exports" 
call foal run create-custodian custodianname="Kool Freight" 
call foal run create-custodian custodianname="Frami and Sons Operations" 

echo create Locations and Sensors (data from Excel)

Rem Test 1
call foal run create-location locationname="Boat 1" locationtype="Mobile" custodianname="Bobs Rocklobsters" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"65850806\", \"85588774\", \"27959163\"]"
call foal run create-location locationname="Truck 1" locationtype="Mobile" custodianname="Bobs Rocklobsters" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"92288196\", \"22973984\", \"25484870\"]"
call foal run create-location locationname="Receiving" locationtype="Fixed" custodianname="Great Seafood" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"86661716\", \"11218886\", \"55694108\"]"
call foal run create-location locationname="Holding Tanks" locationtype="Fixed" custodianname="Great Seafood" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"55208978\", \"3953549\", \"52775229\"]"
call foal run create-location locationname="Processing Line" locationtype="Fixed" custodianname="Great Seafood" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"31843005\", \"42754484\", \"35377957\"]"
call foal run create-location locationname="Warehouse A" locationtype="Fixed" custodianname="Great Seafood" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"31843005\", \"42754484\", \"35377957\"]"
call foal run create-location locationname="Truck 4" locationtype="Mobile" custodianname="Top Shelf Logistics" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"92618577\", \"45458262\", \"26817616\"]"
call foal run create-location locationname="Wholesale Market" locationtype="Fixed" custodianname="Melbourne Seafood Sales" city="West Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"18248544\", \"30178949\", \"38856930\"]"
call foal run create-location locationname="Van 1" locationtype="Mobile" custodianname="Waelchi and Sons Cargo" city="West Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"72873588\", \"27769489\", \"19247905\"]"
call foal run create-location locationname="Restaurant" locationtype="Consumer" custodianname="AJs Bubbles and Seafood bar" city="Southgate" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"93345033\", \"17028688\", \"85604015\"]"
call foal run create-location locationname="Sale - Consumer" locationtype="Consumer" custodianname="AJs Bubbles and Seafood bar" city="Southgate" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"\"]"

Rem Test 2
call foal run create-location locationname="Boat 1" locationtype="Mobile" custodianname="Calm Seas" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"1787992\", \"8698610\", \"7394661\"]"
call foal run create-location locationname="Truck 1" locationtype="Mobile" custodianname="Calm Seas" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"8564650\", \"6812760\", \"8761282\"]"
call foal run create-location locationname="Receiving" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"1796076\", \"1635197\", \"6438891\"]"
call foal run create-location locationname="Holding Tanks" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"6607108\", \"3559940\", \"3931544\"]"
call foal run create-location locationname="Processing Line" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"5377861\", \"5172116\", \"8413380\"]"
call foal run create-location locationname="outbound goods" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="" state="" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"\"]"
call foal run create-location locationname="Truck 4" locationtype="Mobile" custodianname="Kool Freight" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"6108062\", \"9665887\", \"1669432\"]"
call foal run create-location locationname="Melbourne Warehouse" locationtype="Fixed" custodianname="Lennys Supermarket" city="Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"4375257\", \"3011857\", \"9027162\"]"
call foal run create-location locationname="Delivery Truck A" locationtype="Mobile" custodianname="Lennys Supermarket" city="Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"2523478\", \"2307322\", \"5427520\"]"
call foal run create-location locationname="Retail - Kew East" locationtype="Fixed" custodianname="Lennys Supermarket" city="Kew East" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"3054330\", \"5174908\", \"8141793\"]"
call foal run create-location locationname="Sale - Consumer" locationtype="Consumer" custodianname="Lennys Supermarket" city="Kew East" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"2184856\", \"7355782\", \"6494508\"]"


Rem Test 3
call foal run create-location locationname="Boat 1" locationtype="Mobile" custodianname="Calm Seas" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"94292024\", \"24352886\", \"63623427\"]"
call foal run create-location locationname="Truck 1" locationtype="Mobile" custodianname="Calm Seas" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"34287891\", \"83635462\", \"78467036\"]"
call foal run create-location locationname="Receiving" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"88054658\", \"14428851\", \"46284049\"]"
call foal run create-location locationname="Holding Tanks" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"60332659\", \"87875096\", \"29009031\"]"
call foal run create-location locationname="Processing Line" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"25247572\", \"70091390\", \"93681600\"]"
call foal run create-location locationname="outbound goods" locationtype="Fixed" custodianname="Carroll Group Seafoods" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"5377861\", \"5172116\", \"8413380\"]"
call foal run create-location locationname="Truck 7" locationtype="Mobile" custodianname="Low Grade Logistics" city="Port Macdonnell" state="SA" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"36269826\", \"40132819\", \"8087966\"]"
call foal run create-location locationname="Melbourne Warehouse" locationtype="Fixed" custodianname="Lennys Supermarket" city="Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"53810892\", \"99253276\", \"91760731\"]"
call foal run create-location locationname="Delivery Truck C" locationtype="Mobile" custodianname="Lennys Supermarket" city="Melbourne" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"88903465\", \"91740368\", \"96013416\"]"
call foal run create-location locationname="Retail - Kew East" locationtype="Fixed" custodianname="Lennys Supermarket" city="Kew East" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"97402515\", \"8894504\", \"1686727\"]"
call foal run create-location locationname="Sale - Consumer" locationtype="Consumer" custodianname="Lennys Supermarket" city="Kew East" state="VIC" country="Australia" sensortypename="Digital Matter Event" sensorexternalids="[\"23054304\", \"51159784\", \"50134086\"]"


