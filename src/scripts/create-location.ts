/*
foal run create-location locationname="Warehouse A" locationtype="Fixed" custodianname="Company Name" city="Adelaide" state="SA" country="Australia"
*/

// 3p
import { createConnection } from 'typeorm';
import { Location, Custodian, Sensor, Sensortype } from '../app/entities';

export const schema = {
  additionalProperties: false,
  properties: {
	locationname: {type: 'string'},
	locationtype: {type: 'string'}, // fixed | mobile | consumer
	lat: {type: 'number'}, // TODO: upgrade this to PostGIS
	long: {type: 'number'},
	city: {type: 'string'},
	state: {type: 'string'},
	postcode: {type: 'string'},
	country: {type: 'string'},
	custodianid: {type: 'string', format: 'uuid'},
	custodianname: {type: 'string', default: ""},
	sensorexternalids: { type: 'array', default: [] },
	sensortypename: {type: 'string', default: ""},
},
  required: [ 'locationname',  'locationtype' ],
  type: 'object',
};

export async function main(args: any) {
console.log("########## Starting function: " + new Date());
  const connection = await createConnection();
console.log("########## DB connected: " + new Date());

  const locationname = args.locationname;
  const locationtype = args.locationtype;
  const lat = args.lat;
  const long = args.long;
  const city = args.city;
  const state = args.state;
  const postcode = args.postcode;
  const country = args.country;
  const custodianid = args.custodianid;
  const custodianname = args.custodianname;
  const sensortypename = args.sensortypename;
  
  let custodian;
  if(custodianname != undefined && custodianid == undefined) {custodian = await Custodian.findOne({ custodianname: custodianname });} // preference custodian ID over custodian name
  if(custodianid != undefined) {custodian = await Custodian.findOne({ custodianid: custodianid });}
  if(!custodian) {
	  console.error(`custodian ${custodianname} | ${custodianid} does not exist`);
	  await connection.close();
	  return;
  }
  console.log("########## Found custodian: " + new Date());
  const foundcustodianid =  custodian.custodianid;
  let location = await Location.findOne({ where: {locationname: locationname, fk_custodian: {custodianid: foundcustodianid}}, relations: ["fk_custodian"] });
  if(!location) {location = new Location(); console.log("***** Creating new Location");}else{console.log("***** Found Location to update");}

  location.locationname = locationname;
  location.locationtype = locationtype;
  location.lat = lat;
  location.long = long;
  location.city = city;
  location.state = state;
  location.postcode = postcode;
  location.country = country;
  location.fk_custodian = custodian;

  console.log("########## Saving Location: " + new Date());

  try {
	  let savedlocation = await location.save();
	  console.log("########## Creating Sensors: " + new Date());
		let sensortype;
	  if(sensortypename) {
		sensortype = await Sensortype.findOne({where: {sensortypename: sensortypename}});
		if(!sensortype) {console.error("sensortypename not found. cannot create sensors"); return;}
	  }
		for (const sensorexternalid of args.sensorexternalids as string[]) {
		let sensor = await Sensor.findOne({ sensorexternalid });
		if (!sensor) { sensor = new Sensor(); console.log("***** Creating new Sensor");}else{console.log("***** Found Sensor to update");}
		sensor.sensorexternalid = sensorexternalid;
		sensor.fk_location = savedlocation;
		sensor.fk_sensortype = sensortype.sensortypeid;
		await sensor.save();
	  }
	  console.log(savedlocation);
	  console.log("########## Finshed: " + new Date());

  } catch (error) {
    console.error(error);
  } finally {
    await connection.close();
  }
}
