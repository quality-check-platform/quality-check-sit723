// 3p
import { Group, Permission } from '@foal/typeorm';
import { createConnection, getConnection, getManager } from 'typeorm';
import { Sensortype, Eventtype, Sensortype_lnk_eventtype } from '../app/entities';

export const schema = {
	additionalProperties: false,
	properties: {
		sensortypename: { type: 'string' },
		datatype: { type: 'string', default: "json" },
		singularity: { type: 'string', default: "single" },
		sensoridpath: { type: 'string' },
		datamap: { type: 'string' },
		eventtypename: { type: 'string' },
	},
	required: ["sensortypename", "sensoridpath", "datamap", "eventtypename"],
	type: 'object',
};

export async function main(args: any) {
	const connection = await createConnection();
	const sensortypename = args.sensortypename;
	const datatype = args.datatype;
	const singularity = args.singularity;
	const sensoridpath = args.sensoridpath;
	let datamap = args.datamap;
	const eventtypename = args.eventtypename;
	if(datamap.charAt(0) == "$") {datamap = datamap.slice(1, datamap.length);}

	let sensortype = await Sensortype.findOne({ where: { sensortypename: sensortypename } });
	if (!sensortype) { sensortype = new Sensortype(); console.log("***** Creating new Sensortype"); } else { console.log("***** Found Sensortype to update"); }
	//TODO: grab the old version if updating to use for the linking table query
	sensortype.sensortypename = sensortypename;
	sensortype.datatype = datatype;
	sensortype.singularity = singularity;
	sensortype.sensoridpath = sensoridpath;

	let savedsensortype;
	try {
		savedsensortype = await sensortype.save();
		console.log("sensortype saved: " + savedsensortype.sensortypeid)
	} catch (error) {
		console.error(error);
		await connection.close();
		return;
	}

	let eventtype = await Eventtype.findOne({ where: { eventtypename: eventtypename } });
	if (!eventtype) { eventtype = new Eventtype(); console.log("***** Creating new eventtype"); } else { console.log("***** Found eventtype to update"); }
	//TODO: grab the old version if updating to use for the linking table query
	eventtype.eventtypename = eventtypename;
	let savedeventtype;
	try {
		savedeventtype = await eventtype.save();
		console.log("eventtype saved: " + savedeventtype.eventtypeid)
	} catch (error) {
		console.error(error);
		await connection.close();
		return;
	}

	const sensoreventtypename = sensortypename + "-" + eventtypename;
	let sensortype_lnk_eventtype = await Sensortype_lnk_eventtype.findOne({ where: { sensoreventtypename: sensoreventtypename } });
	if (!sensortype_lnk_eventtype) { sensortype_lnk_eventtype = new Sensortype_lnk_eventtype(); console.log("***** Creating new sensortype_lnk_eventtype"); } else { console.log("***** Found sensortype_lnk_eventtype to update"); }
	sensortype_lnk_eventtype.sensoreventtypename = sensoreventtypename;
	sensortype_lnk_eventtype.fk_eventtype = savedeventtype.eventtypeid;
	sensortype_lnk_eventtype.fk_sensortype = savedsensortype.sensortypeid;
	sensortype_lnk_eventtype.datamap = datamap;
	try {
		const savedsensortype_lnk_eventtype = await sensortype_lnk_eventtype.save();
		console.log("sensortype_lnk_eventtype saved: " + savedsensortype_lnk_eventtype.id)
	} catch (error) {
		console.error(error);
		return;
	} finally {
		await connection.close();
	}
}
