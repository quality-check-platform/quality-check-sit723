# Quality Check Platform BE
## Contact
Anthony Walker
anthonyjarviswalker@gmail.com
Last updated 2021-10-10

## Latest Version
This is a snapshot of work completed under Deakin University subject SIT723
For the latest version including any further work completed please refer to: https://gitlab.com/quality-check-platform/quality-check-be

# Environment Setup
## Install requirements
Install Postgres

Install Docker Desktop

Install Postgres ODBC driver:
https://www.postgresql.org/ftp/odbc/versions/msi/

Install node.js
Install Git

## Creating the database
Create a new DB in Postgres
name: internetoffood
create a schema in this DB: systemmaster

## Create InfluxDB bucket
Access InfluxDB UI via Docker Desktop
Create a new Bucket and add teh details to .env file

## Get services and code
docker pull redis
	Container Name: redis
	Map port: 6379
	no local storage required

docker pull influxdb
	Container Name: influxdb
	map port 8086
	map local folder: C:\code\qualitycheckplatform\DockerData\influxdb to Container path: /var/lib/influxdb2/
	Ref: https://docs.influxdata.com/influxdb/v2.0/upgrade/v1-to-v2/docker/#file-system-mounts
	Ref: InfluxDB reference: https://influxdata.github.io/influxdb-client-js/influxdb-client.point.html

clone the repo

## Setup, build and start
setup .env file with the values as per .env.template

Install the dependencies:
npm i

Build the code
npm run build

Seed the DB as per below

Start the server:
npm start

Execute the tests as per below.

# Seeding Data
All Seeding data is sourced from Test Data Generator.xlsx
refer columns highlighted in pale green for commands to execute
Seed the system in the following order:
Sensor Types
Products
Product Alarms
Product Quality Models
Custodians
Locations (inc sensors)

# Running the system (Executing tests)
All Testing data is sourced from Test Data Generator.xlsx
Refresh barcode data using the randon number generator at the top of the column
execute the commands in the Commands column (highlighted in orange)

# Accessing the consumer UI
open http://localhost:3001
Refresh the data in the Test Data Generator.xlsx to get the UUIDs for the batches

# Configuring sensors:
Refer JSONPath
https://www.npmjs.com/package/jsonpath
https://jsonpath.com/


# TODO
## Bearer toker setup:
https://foalts.org/docs/authentication-and-access-control/quick-start
token: ctx.session.getToken()

## PostGIS
https://stackoverflow.com/questions/65041545/how-can-i-use-longitude-and-latitude-with-typeorm-and-postgres

## MQTT:
https://www.npmjs.com/package/mqtt
http://www.steves-internet-guide.com/using-node-mqtt-client/


